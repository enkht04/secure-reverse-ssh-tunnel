#!/bin/bash

REMOTE_USER="root"

function check_if_running_as_root {
    if [[ $EUID -ne 0 ]]; then
        echo "This script must be run as root."
        exit 1
    fi

    echo "OK: Root user detected."
}

check_if_running_as_root

read -p "Are you running this script on the host to expose? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Please re-run this script on HTE as root user."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

read -p "Have you setup targets and adjusted configurations as described in README?" -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Please read README and rerun this script."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

function check_necessary_packages {
    echo "NEXT: Checking for valid package manager."

    APT_GET_CMD=$(which apt-get)
    YUM_CMD=$(which yum)
    DNF_CMD=$(which dnf)

    if [[ ! -z $APT_GET_CMD ]]; then
        echo "OK: apt-get found."
        apt-get --yes install autossh
    elif [[ ! -z $YUM_CMD ]]; then
        echo "OK: yum found."
        yum install -y autossh
    elif [[ -z $DNF_CMD ]]; then
        echo "OK: dnf found."
        dnf install -y autossh
    else
        echo "No valid package manager found. Exiting."
        exit 1;
    fi

    echo "OK: Autossh installed"
}

function check_for_autossh_user {
    echo "NEXT: Check for existence of autossh dedicated user."

    if [ ! id -u autossh >/dev/null 2>&1 ]; then
        echo "The user is missing so we will create for you."
        useradd -m -s /bin/false autossh
    fi

    if [ ! id -u autossh >/dev/null 2>&1 ]; then
        echo "There are some problems with user creation. Exiting."
        exit 1;
    fi
}

function adjust_ssh_folder_for {
    homedir=$( getent passwd $REMOTE_USER | cut -d: -f6 )

    echo "NEXT: Setup ${1} home: ${homedir}."

    mkdir -p "${homedir}/.ssh" &&
    touch -a "${homedir}/.ssh/authorized_keys"

    if [ ! -s authorized_keys ]; then
        echo "WARNING: authorized_keys in setup folder seems empty so you should manually setup host authorized_keys or rerun this script."
    fi
    cat authorized_keys >> "${homedir}/.ssh/authorized_keys"

    echo -e "OK: Files and content ready.\nNEXT: Setup file and folder permissions."
    
    chown -R "$1":"$1" "${homedir}/.ssh"
    chmod 700 "${homedir}/.ssh"
    chmod 600 "${homedir}/.ssh/authorized_keys"

    echo "OK: File and folder permissions setup."
}

function setup_systemd_service_if_available {
    echo "NEXT: Checking for systemd."

    SYSTEMCTL_CMD=$(which systemctl)
    if [[ ! -z $SYSTEMCTL_CMD ]]; then
        echo "NEXT: Copy targets into /etc/default."

        cp -n targets/* /etc/default/ &&

        echo "OK: Targets copied. \nNEXT: Setup systemd service."

        cp secure-tunnel@.service /etc/systemd/system/ &&
        systemctl daemon-reload

        echo "OK: Systemd service created."
    else
        echo "WARNING: No systemd installation found. You should manually setup an autossh service to keep tunnel alive."
    fi
}

check_necessary_packages
check_for_autossh_user

adjust_ssh_folder_for "${REMOTE_USER}"
setup_systemd_service_if_available

echo -e "All done. Remaining steps:\n
- generate an ssh keypair with ssh-keygen for user autossh and push pubkey to the jump server
	i.e.	ssh-keygen -f autossh@"$jumpserver-ip" -c "$USER@$HOSTNAME" && ssh-copy-id -i autossh@"$jumpserver-ip" user@"$jumpserverip" 

- Edit JumpHost's /etc/ssh/sshd_config with

Match User autossh
   AllowTcpForwarding yes
   X11Forwarding no
   PermitTunnel yes
   GatewayPorts no
   AllowAgentForwarding no

- configure your client's (EUD) ~/.ssh/config like the provided one with this repo \n"
